#pragma once
#include <iostream>
#include <string>
#include <functional>
#include <cstdio>

// typedef std::function<void* (void*)> fun_t;
typedef void *(*fun_t)(void *);

class ThreadData
{
public:
    void *args_;
    std::string name_;
};

class Thread
{
public:
    // 线程池会创建多个Thread对象,存在vector中,每创建一个thread对象都会传入，线程要调用的方法和线程所需要的东西(放在args中)
    Thread(int num, fun_t callback, void *args) : func_(callback)
    {
        char nameBuffer[64];
        snprintf(nameBuffer, sizeof nameBuffer, "Thread-%d", num);
        name_ = nameBuffer;

        // 下面是对args的二次封装,添加一些东西给func_函数, 本来要给args,但是想添加一个name_给func_,所以对其args进行了改造
        tdata_.args_ = args;
        tdata_.name_ = name_;
    }

    void start()
    {
        pthread_create(&tid_, nullptr, func_, (void*)&tdata_);
    }

    void join()
    {
        pthread_join(tid_, nullptr);
    }

    std::string name()
    {
        return name_;
    }
    
    ~Thread()
    {}

private:
    std::string name_;
    fun_t func_;
    ThreadData tdata_;
    pthread_t tid_;
};