#pragma once
#include<pthread.h>


class Mutex
{
public:
    Mutex(pthread_mutex_t* mtx):_mtx(mtx)
    {}

    void lock()
    {
        pthread_mutex_lock(_mtx);
    }

    void unlock()
    {
        pthread_mutex_unlock(_mtx);
    }

    ~Mutex()
    {}
    
private:
    pthread_mutex_t* _mtx;
};


// RALL
class lockGuard
{
public:
    lockGuard(pthread_mutex_t* mtx):_mutex(mtx)
    {
        _mutex.lock();
    }

    ~lockGuard()
    {
        _mutex.unlock();
    }
private:
    Mutex _mutex;
};