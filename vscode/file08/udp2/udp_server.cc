#include "udp_server.hpp"
#include <memory>

static void usage(std::string proc)
{
    std::cout<<"\nUsage: " << proc << " port\n" <<std::endl;
}


//【绑定任意ip，服务端server只需要port】
// ./udp_server port
int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        usage(argv[0]); // 出错将使用手册打印出来
        exit(1);
    }

    uint16_t port = atoi(argv[1]);
    std::unique_ptr<UdpServer> svr(new UdpServer(port));
    
    svr->initServer();
    svr->start();
    return 0;
}