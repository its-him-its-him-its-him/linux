#include "udp_server.hpp"
#include <memory>

static void usage(std::string proc)
{
    // std::cout<<"\nUsage: " << proc << " ip port\n" <<std::endl; // server绑定任意ip只需要port就可以了
    std::cout<<"\nUsage: " << proc << " port\n" <<std::endl;
}

// // ./udp_server ip port
// int main(int argc, char* argv[])
// {
//     if(argc != 3)
//     {
//         usage(argv[0]); // 出错将使用手册打印出来
//         exit(1);
//     }

//     std::string ip = argv[1];
//     uint16_t port = atoi(argv[2]);
//     std::unique_ptr<UdpServer> svr(new UdpServer(port, ip));
    
//     svr->initServer();
//     svr->start();
//     return 0;
// }

//【绑定任意ip，服务端server只需要port】
// ./udp_server port
int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        usage(argv[0]); // 出错将使用手册打印出来
        exit(1);
    }

    uint16_t port = atoi(argv[1]);
    std::unique_ptr<UdpServer> svr(new UdpServer(port));
    
    svr->initServer();
    svr->start();
    return 0;
}