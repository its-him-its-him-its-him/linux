// #include <signal.h>
// #include <iostream>
// #include <string>
// #include "./common/httplib.h"

// using namespace httplib;

// int main()
// {
//     // svr.set_keep_alive_max_count(2); // Default is 5  Keep-Alive connection
//     // svr.set_keep_alive_timeout(10);  // Default is 5
//     // svr.set_read_timeout(5, 0); // 5 seconds
//     // svr.set_write_timeout(5, 0); // 5 seconds
//     // svr.set_idle_interval(0, 100000); // 100 milliseconds

//     // cli.set_connection_timeout(0, 300000); // 300 milliseconds
//     // cli.set_read_timeout(5, 0); // 5 seconds
//     // cli.set_write_timeout(5, 0); // 5 seconds

//     Server svr;
//     svr.set_keep_alive_max_count(2);
//     svr.set_read_timeout(5, 0); // 5 seconds
//     svr.set_write_timeout(5, 0); // 5 seconds
//     svr.set_idle_interval(0, 10000); // 100 milliseconds
//     svr.set_keep_alive_timeout(10);  // Default is 5
//     svr.Get("/hi", [](const Request &, Response &res)
//     { 
//         res.set_content("Hello World!", "text/plain"); 
        
//     });

//     svr.listen("0.0.0.0", 8080);
//     return 0;
// }

// #include <iostream>
// #include <string>
// // #include <vector>
// // #include <boost/algorithm/string.hpp>
// // #include <ctemplate/template.h>
// #include "./common/httplib.h"

// int main()
// {
//     httplib::Client cli("127.0.0.0", 8080);
//     auto res = cli.Post("/hi");
//     std::cout << res << std::endl;
//     if (res)
//     {
//         if (res->status == 200)
//         {
//             std::cout << res->body << std::endl;
//         }
//     }
//     else
//     {
//         auto err = res.error();
//     }
//     return 0;
// }

// int main()
// {
//     // 要渲染的html页面，文件所在路径
//     std::string html = "./text.html";
//     // 1.建立ctemplate参数目录结构 [相当于创建一个可以存储kv结构的对象，对象名字是text]
//     ctemplate::TemplateDictionary root("test");
//     // 向结构中添加你要替换的数据，kv的
//     root.SetValue("information", "测试ctemplate");

//     // 2.获取被渲染对象
//     ctemplate::Template *tpl = ctemplate::Template::GetTemplate(html,\
// ctemplate::DO_NOT_STRIP); //DO_NOT_STRIP：保持html网页原貌

//     //3.开始渲染，返回新的网页结果到out_html
//     std::string out_html;
//     tpl->Expand(&out_html, &root);

//     // std::cout << "原本html是:" << std::endl;
//     // std::cout << html << std::endl;
//     std::cout << "渲染的带参html是:" << std::endl;
//     std::cout << out_html << std::endl;

//     return 0;
// }

// int main()
// {
//     std::vector<std::string> tokens;
//     const std::string str = "1::判断回文数::简单::1::30000";
//     const std::string sep = ":";
//     // boost::split(tokens, str, boost::is_any_of(sep), boost::algorithm::token_compress_off); // off代表不压缩
//     // 判断回文数

//     // 简单

//     // 1

//     // 30000

//     boost::split(tokens, str, boost::is_any_of(sep), boost::algorithm::token_compress_on); // on代表压缩
//     // 1
//     // 判断回文数
//     // 简单
//     // 1
//     // 30000
//     for(auto &iter : tokens)
//     {
//         std::cout << iter << std::endl;
//     }
//     return 0;
// }

// #include <iostream>
// #include <unistd.h>
// #include <jsoncpp/json/json.h>
// // find 搜索路径 [选项] 搜索内容
// // -name: 按照文件名搜索
// // find /usr/ -name json.h

// int main()
// {
//     // 序列化: 将结构化数据转化为一个字符串

//     //Value是一个Json的中间类，可以填充KV值
//     Json::Value root1;
//     root1["aaaa"] = "11111";
//     root1["bbbb"] = "22222";

//     // Json::StyledWriter writer;
//     Json::FastWriter writer;
//     std::string str = writer.write(root1);
//     std::cout << str <<std::endl;
//     // StyledWriter: 格式
//     // {
//     //     "aaaa" : "11111",
//     //     "bbbb" : "22222",
//     // }

//     // FastWriter: 格式
//     // {"aaaa":"11111","bbbb":"22222"}

//     // 反序列化: 将json式的字符串转为结构化数据

//     // 外界给一个 json 类型的 字符串 str: 比如下面
//     // {"aaaa":"11111","bbbb":"22222"}

//     // 需要将 str 转为 json式的结构化数据: 比如下面
//     // root1["aaaa"] = "11111";
//     // root1["bbbb"] = "22222";

//     Json::Value root2;
//     Json::Reader reader;

//     // bool parse(const std::string& document, Value& root, bool collectComments = true);
//     bool ret = reader.parse(str, root2);
//     // 如何提取
//     std::string s1 = root2["aaaa"].asString();
//     std::string s2 = root2["bbbb"].asString();

//     return 0;
// }

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/signal.h>
#include <unistd.h>

// 演示CPU与内存资源而导致进程终止，OS会发生什么信号
void handler(int signo)
{
    std::cout << "signo : " << signo << std::endl;
    exit(1);
}

int main()
{
    //资源不足，导致OS终止进程，是通过信号终止的
    for(int i =1; i <= 31; i++){
        signal(i, handler);
    }

    // // 限制累计运行时长
    // struct rlimit r;
    // r.rlim_cur = 1;
    // r.rlim_max = RLIM_INFINITY;
    // setrlimit(RLIMIT_CPU, &r);
    // while(1);

    struct rlimit r;
    r.rlim_cur = 1024 * 1024 * 40; //20M
    r.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_AS, &r);

    int count = 0;
    while(true)
    {
        int *p = new int[1024*1024];
        count++;
        std::cout << "size: " << count << std::endl;
        sleep(1);
    }
    return 0;
}