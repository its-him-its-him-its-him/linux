#include <iostream>
#include <signal.h>


#include "../common/httplib.h"
#include "oj_control.hpp"

using namespace httplib;
using namespace ns_control;

static Control *ctrl_ptr = nullptr;

void RecoveryAllHost(int sig)
{
    ctrl_ptr->RecoveryHost();
}

int main()
{
    signal(SIGQUIT, RecoveryAllHost);
    
    // 用户请求服务的路由功能
    Server svr;
    Control ctrl;
    ctrl_ptr = &ctrl;

    /**********************************
     * 首先需要考虑: 用户oj 请求包含三部分
     * 1. 求情题目列表 2. 用户点击题目列表中的某一道题目，跳转到题目指定的页面 3. 提交代码显示的页面
    **********************************/
    
   // 1.获取所有的题目列表
    svr.Get("/all_questions", [&ctrl](const Request &req, Response &resp){
        //返回一张包含有所有题目的html网页
        std::string html;
        ctrl.AllQuestions(&html);
        //用户看到的是 拼上了相关题目数据 的网页数据
        resp.set_content(html, "text/html; charset=utf-8");
    });


   // 2. 根据题目编号。获取题目内容
   // 正则表达式: /Question/(\d+) ; 
   // matches获取url请求输入的字符信息，matches[0]是指(\d+)
   // R"()", 原始字符串raw string,保持字符串内容的原貌，不用做相关的转义
    svr.Get(R"(/question/(\d+))", [&ctrl](const Request& req, Response& resp){
        std::string number = req.matches[1];
        //返回一张某个题目的html网页
        std::string html;
        ctrl.Question(number, &html);
        resp.set_content(html, "text/html; charset=utf-8");
        
    });

    // // 3. 提交代码，显示的页面
    // 用户提交代码，使用我们的判题功能(1. 每道题的测试用例 2. compile_and_run)
    svr.Post(R"(/judge/(\d+))", [&ctrl](const Request &req, Response &resp){
        std::string number = req.matches[1];
        std::string result_json;
        ctrl.Judge(number, req.body, &result_json);
        resp.set_content(result_json, "application/json;charset=utf-8");
    });
    
    svr.set_base_dir("./wwwroot");

    svr.listen("0.0.0.0", 8080);


    return 0;
}