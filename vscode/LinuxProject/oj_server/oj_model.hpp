// #pragma once
// #include <unordered_map>
// #include <string>
// #include <iostream>
// #include <fstream>
// #include <vector>

// #include "../common/log.hpp"
// #include "../common/util.hpp"

// namespace ns_model
// {
//     using namespace ns_log;
//     using namespace ns_util;

//     struct Question
//     {
//         std::string number; //题目编号，唯一
//         std::string title;  //题目的标题
//         std::string star;   //难度: 简单 中等 困难
//         int cpu_limit;      //题目的时间要求(S)
//         int mem_limit;      //题目的空间要去(KB)
//         std::string desc;   //题目的描述
//         std::string header;   //题目预设给用户在线编辑器的代码(部分)
//         std::string tail; //题目的测试用例，需要和header拼接，形成完整代码
//     };

//     // 数据文件
//     const std::string questions_path = "./questions/";
//     // 数据文件中文件列表所在路径
//     const std::string questins_list = "./questions/questions.list";

//     class Model
//     {
//     private:
//         std::unordered_map<std::string, Question> questions;
//     public:
//         Model()
//         {
//             assert(LoadQuestionList(questins_list));
//         }
//     public:
//         bool LoadQuestionList(const std::string &question_list)
//         {
//             // 加载配置文件，将题目列表里面所有的 题目编号:题目属性，保存在undordered_map中
//             std::ifstream in(question_list);
//             if(!in.is_open())
//             {
//                 LOG(FATAL) << " 加载题库列表失败" << "\n";
//                 return false;
//             }

//             std::string line;
//             while(getline(in, line))
//             {
//                 std::vector<std::string> splits;
//                 StringUtil::SplitString(line, &splits, " ");
//                 // 1 判断回文数 简单 1 51200
//                 if(splits.size() != 5)
//                 {
//                     LOG(WARNING) << "加载题目信息失败, 请检查题目格式" << "\n";
//                     continue;
//                 }

//                 Question q;
//                 q.number = splits[0];
//                 q.title = splits[1];
//                 q.star = splits[2];
//                 q.cpu_limit = atoi(splits[3].c_str());
//                 q.mem_limit = atoi(splits[4].c_str());

//                 // ./questions/1
//                 std::string path = questions_path;
//                 path += q.number;
//                 path += "/";

//                 // 读取path路径下题目文件
//                 FileUtil::ReadFile(path+"desc.txt", &(q.desc), true);
//                 FileUtil::ReadFile(path+"header.cpp", &(q.header), true);
//                 FileUtil::ReadFile(path+"tail.cpp", &(q.tail), true);

//                 questions.insert({q.number, q});
//             }
//             LOG(INFO) << "加载题库...成功!" << "\n";
//             in.close();

//             return true;
//         }

//         // 题目全部保存在unordered_map中
//         bool GetAllQuestions(std::vector<Question> *out)
//         {
//             if(questions.size() == 0)
//             {
//                 LOG(ERROR) << "用户获取题库失败" << "\n";
//                 return false;
//             }
//             for(const auto &q : questions)
//             {
//                 out->push_back(q.second); //first: key, second: value
//             }
//             // std::cout << "oj_mode::GetAllQuestions()" << std::endl;
//             return true;
//         }
//         bool GetOneQuestion(const std::string &number, Question *q)
//         {
//             // std::cout << number << std::endl;
//             const auto& iter = questions.find(number);
//             if(iter == questions.end())
//             {
//                 LOG(ERROR) << "用户获取题目失败, 题目编号: " << number << "\n";
//                 return false;
//             }
//             (*q) = iter->second;
//             return true;
//         }
//     };
// }