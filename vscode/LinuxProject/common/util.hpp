#pragma once

// util： 提供一个工具类

#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <vector>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <atomic>
#include <fstream>
#include <boost/algorithm/string.hpp>

namespace ns_util
{
    const std::string temp_files_path = "./temp_files/";

    // 时间工具类(获取时间戳)
    class TimeUtil
    {
    public:
        // int gettimeofday(struct timeval *tv, struct timezone *tz);
        //  struct timeval 
        //  {
        //      time_t      tv_sec;     /* seconds */秒
        //      suseconds_t tv_usec;    /* microseconds */微秒
        //  };

        // 获得秒时间戳
        static std::string GetTimeStamp()
        {
            struct timeval _time;
            gettimeofday(&_time, nullptr);
            return std::to_string(_time.tv_sec);
        }
        //获得毫秒时间戳
        static std::string GetTimeMs()
        {
            struct timeval _time;
            gettimeofday(&_time, nullptr);
            return std::to_string(_time.tv_sec * 1000 + _time.tv_usec / 1000);
        }
    };

    // 对文件路径操作
    class PathUtil
    {
    public:
        // 添加后缀
        static std::string AddSuffix(const std::string& filename, const std::string& suffix)
        {
            std::string path_name = temp_files_path;
            path_name += filename;
            path_name += suffix;
            return path_name;
        }
        // 根据给传入的文件添加后缀，形成源文件(filename.cc)
        static std::string SrcFile(const std::string& filename)
        {
            return AddSuffix(filename, ".cpp");
        }
        // 根据给传入的文件添加后缀，形成可执行文件(filename.exe)
        static std::string ExeFile(const std::string& filename)
        {
            return AddSuffix(filename, ".exe");
        }
        // 根据给传入的文件添加后缀，形成stderr文件(filename.stderr) 里面存放编译出错的信息
        static std::string StderrFile(const std::string& filename)
        {
            return AddSuffix(filename, ".compiler_stderr");
        }


        // 运行时需要的三个临时文件
        static std::string Stdin(const std::string& filename)
        {
            return AddSuffix(filename, ".stdin");
        }
        static std::string Stdout(const std::string& filename)
        {
            return AddSuffix(filename, ".stdout");
        }
        static std::string Stderr(const std::string& filename)
        {
            return AddSuffix(filename, ".runner_stderr");
        }
    };
   
    // 对文件操作
    class FileUtil
    {
    public:
        // 判断文件是否存在
        static bool IsFileExist(const std::string& path_name)
        {
            // int stat(const char *path, struct stat *buf);
            // stat: 获取文件的属性(buf是输出型参数) 获取到代表存在否则不存在
            struct stat st;
            if(stat(path_name.c_str(), &st) == 0)
                return true;
            return false;
        }
        // 对文件读
        static bool ReadFile(const std::string &target, std::string *content, bool keep = false)
        {
            (*content).clear();

            std::ifstream in(target);
            if (!in.is_open())
            {
                return false;
            }
            std::string line;
            // getline:不保存行分割符,有些时候需要保留\n,
            // getline内部重载了强制类型转化
            while (std::getline(in, line))
            {
                (*content) += line;
                (*content) += (keep ? "\n" : "");
            }
            in.close();
            return true;
        }
        // 对文件写
        static bool WriteFile(const std::string &target, const std::string &content)
        {
            std::ofstream out(target);
            if (!out.is_open())
            {
                return false;
            }
            out.write(content.c_str(), content.size());
            out.close();
            return true;
        }
        // 获取唯一的文件名
        static std::string UniqFileName()
        {
            static std::atomic_uint id(0);
            id++;
            // 毫秒级时间戳+原子性递增唯一值: 来保证唯一性
            std::string ms = TimeUtil::GetTimeMs();
            std::string uniq_id = std::to_string(id);
            return ms + "_" + uniq_id;
        }

    };
 
    // 对字符串的操作
    class StringUtil
    {
    public:
        /*************************************
         * str: 输入型，目标要切分的字符串
         * target: 输出型，保存切分完毕的结果
         * sep: 指定的分割符
         * **********************************/
        static void SplitString(const std::string &str, std::vector<std::string> *target, const std::string &sep)
        {
            //boost split
            boost::split((*target), str, boost::is_any_of(sep), boost::algorithm::token_compress_on);
        }
    };
}