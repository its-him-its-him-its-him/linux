#pragma once

#include <signal.h>
#include <unistd.h>

#include "compiler.hpp"
#include "runner.hpp"
#include "jsoncpp/json/json.h"
#include "../common/log.hpp"
#include "../common/util.hpp"



namespace ns_compiler_runner
{
    using namespace ns_compiler;
    using namespace ns_runner;
    using namespace ns_util;
    
    class CompileAndRun
    {
    public:
        static void RemoveTempFile(const std::string &file_name)
        {
            //清理文件的个数是不确定的，因为编译和运行时某处发生错误，可能就不会产生某个临时文件
            //但是我们知道最多会产生多少个临时文件，只需要判断就可以了，存在就删除
            std::string _src = PathUtil::SrcFile(file_name);
            if(FileUtil::IsFileExist(_src)) unlink(_src.c_str());

            std::string _compiler_error = PathUtil::StderrFile(file_name);
            if(FileUtil::IsFileExist(_compiler_error)) unlink(_compiler_error.c_str());

            std::string _execute = PathUtil::ExeFile(file_name);
            if(FileUtil::IsFileExist(_execute)) unlink(_execute.c_str());

            std::string _stdin = PathUtil::Stdin(file_name);
            if(FileUtil::IsFileExist(_stdin)) unlink(_stdin.c_str());

            std::string _stdout = PathUtil::Stdout(file_name);
            if(FileUtil::IsFileExist(_stdout)) unlink(_stdout.c_str());

            std::string _stderr = PathUtil::Stderr(file_name);
            if(FileUtil::IsFileExist(_stderr)) unlink(_stderr.c_str());
        }
        /********************************
         * code > 0 : 进程收到了信号导致异常奔溃
         * code < 0 : 整个过程非运行报错(代码为空，编译报错等)
         * code = 0 : 整个过程全部完成
         * 后续可以继续添加其它错误
        *********************************/
        static std::string CodeToDesc(int code, const std::string &file_name)
        {
            std::string desc;
            switch (code)
            {
            case 0:
                desc = "编译运行成功";
                break;
            case -1:
                desc = "提交的代码是空";
                break;
            case -2:
                desc = "未知错误";
                break;
            case -3:
                // 代码编译的时候发生了错误，需要将编译错误文件填入des
                FileUtil::ReadFile(PathUtil::StderrFile(file_name), &desc, true);
                break;
            case SIGABRT: // 6
                desc = "内存超过范围";
                break;
            case SIGXCPU: // 24
                desc = "CPU使用超时";
                break;
            case SIGFPE: // 8
                desc = "浮点数溢出";
                break;
            default:
                desc = "未知: " + std::to_string(code);
                break;
            }

            return desc;
        }
        /***********************************
         * 这个类会调用编译和运行
         * 1.首先需要获取一份可以编译的代码
         * 2.这份代码是以json方式传入Start函数
         * 3.编译和运行后需要将结果再以jsonf方式返回去
         * 4.所以需要两个参数，一个输入参数，一个输出参数
         * 
         * 这里做一个规定
         * 输入:
         * code: 用户提交的代码
         * input:用户提交的代码对应的输入
         * cpu_limit:时间要求（运行需要）
         * mem_limit:空间要求（运行需要）
         * 输出:
         * status:状态码
         * reason:请求的结果
         * 
        ***********************************/
        static void Start(const std::string &in_json, std::string *out_json)
        {
            Json::Value in_value;
            Json::Reader reader;
            // 将传入的in_json字符串转为结构化json放在 in_value中
            reader.parse(in_json, in_value);

            std::string code = in_value["code"].asString();
            std::string input = in_value["input"].asString();
            int cpu_limit = in_value["cpu_limit"].asInt();
            int mem_limit = in_value["mem_limit"].asInt();

            // 注意: 使用End中间不能定义变量,需要放在外面
            int status_code = 0;
            Json::Value out_value;
            int run_result = 0;
            std::string OnlyFileName; //需要内部形成的唯一文件名

            // 判断code是否为空
            if(code.size() == 0)
            {
                status_code = -1; //代码为空
                goto END;
            }


            // 调用编译需要传入一个文件名
            // 首先需要创建一个文件将code放入
            // 需要注意形成的文件需要具有唯一性，防止创建到相同的文件
            // 方法: 毫秒级时间戳+原子性递增唯一值: 来保证唯一性
            OnlyFileName =  FileUtil::UniqFileName();

            // 向文件写入code,形成一个源文件
            if (!FileUtil::WriteFile(PathUtil::SrcFile(OnlyFileName), code))
            {
                status_code = -2; //未知错误
                goto END;
            }

            // 编译
            if (!Compiler::Compile(OnlyFileName))
            {
                //编译失败
                status_code = -3; //代码编译的时候发生了错误
                goto END;
            }

            // 运行
            run_result = Runner::Run(OnlyFileName, cpu_limit, mem_limit);
            if (run_result < 0)
            {
                status_code = -2; //未知错误
            }
            else if (run_result > 0)
            {
                //程序运行崩溃了
                status_code = run_result;
            }
            else
            {
                //运行成功
                status_code = 0;
            }
        END:
            out_value["status"] = status_code;
            out_value["reason"] = CodeToDesc(status_code, OnlyFileName);
            if (status_code == 0)
            {
                // 整个过程全部成功
                std::string _stdout;
                FileUtil::ReadFile(PathUtil::Stdout(OnlyFileName), &_stdout, true);
                out_value["stdout"] = _stdout;

                std::string _stderr;
                FileUtil::ReadFile(PathUtil::Stderr(OnlyFileName), &_stderr, true);
                out_value["stderr"] = _stderr;
            }

            Json::StyledWriter writer;
            *out_json = writer.write(out_value);

            RemoveTempFile(OnlyFileName); 
        }
    };
}