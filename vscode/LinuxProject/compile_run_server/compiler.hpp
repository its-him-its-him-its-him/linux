#pragma once

// 此hpp只负责代码编译

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


#include "../common/util.hpp"
#include "../common/log.hpp"


namespace ns_compiler
{
    using namespace ns_util;
    using namespace ns_log;

    class Compiler
    {
    public:
        Compiler()
        {}

        ~Compiler()
        {}
    public:
        // 根据传入的文件进行编译，返回bool类型判断编译是否成功
        static bool Compile(const std::string& filename)
        {
            
            pid_t id = fork();
            if(id < 0)
            {
                LOG(ERROR) <<"创建子进程失败"<<"\n";
                return false;
            }
            else if(id == 0)
            {
                umask(0);
                // 子进程:将编译错误写到错误文件当中 -- 重定向
                int fd_stderr = open(PathUtil::StderrFile(filename).c_str(), O_CREAT | O_WRONLY, 0644);
                if(fd_stderr < 0)
                {
                    LOG(WARNING) << "编译没有形成stderr文件"<<"\n";
                    exit(1);
                }
                
                // 2重定向到fd_stderr
                dup2(fd_stderr, 2);

                // 程序替换不影响文件描述符表，替换后上面重定向任然有效
                // 子进程: 调用编译器完成编译
                // g++ -o xx.exe xx.cpp -std=c++11 
                // 使用程序替换: execlp不需要带路径
                execlp("g++", "g++","-o", PathUtil::ExeFile(filename).c_str(),\
                PathUtil::SrcFile(filename).c_str(), "-std=c++11", "-D COMPILER_ONLINE", nullptr);

                LOG(ERROR) << "程序替换, 编译g++失败" <<"\n";
                exit(2);
            }
            else 
            {
                waitpid(id, nullptr, 0); // 不关心退出结果且阻塞等待
                // 编译是否成功: 即形成可执行程序是否存在
                if(FileUtil::IsFileExist(PathUtil::ExeFile(filename)))
                {
                    LOG(INFO) << PathUtil::SrcFile(filename).c_str() << "编译成功" << "\n";
                    return true;
                }

                // 走到这里代表编译失败，没有形成可执行程序
                LOG(DEBUG) << PathUtil::SrcFile(filename).c_str() << "\n";
                LOG(INFO) <<  "编译失败, 没有形成可执行程序" << "\n";
                
                // 需要将错误信息打印到stderr文件里面
                return false;
            }
        }

    };
}