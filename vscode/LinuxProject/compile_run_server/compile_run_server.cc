#include "compiler_runner.hpp"
#include "../common/httplib.h" // 阻塞式

using namespace ns_compiler_runner;
using namespace httplib;

void Usage(std::string proc)
{
    std::cerr << "Usage: " << "\n\t" << proc << " port" << std::endl;
}

int main(int argc, char*argv[])
{

    if(argc != 2)
    {
        Usage(argv[0]);
        return 1;
    }

    Server svr;

    // // 根据/good调用后面的lambal表达式
    // // set_content 前面是内用，后面是内容的类型
    // svr.Get("/good", [](const Request& req, Response& resp){
    //     resp.set_content("good morning!!宋增", "text/plain;charset=UTF-8");
    // });

    svr.Post("/compile_and_run", [](const Request &req, Response &resp){
        // 用户请求的服务正文是我们想要的json string
        std::string in_json = req.body;
        std::string out_json;
        if(!in_json.empty())
        {
            CompileAndRun::Start(in_json, &out_json);
            resp.set_content(out_json, "application/json;charset=utf-8");
        }
    });
    
   //svr.set_base_dir("./wwwroot"); // 首页信息
    svr.listen("0.0.0.0", atoi(argv[1]));  // 启动http服务
    return 0;
}






// int main()
// {

//     // in_json: {"code": "#include...", "input": "","cpu_limit":1, "mem_limit":10240}
//     // out_json: {"status":"0", "reason":"","stdout":"","stderr":"",}
//     std::string in_json;
//     Json::Value in_value;

//     in_value["input"] = "";
//     in_value["cpu_limit"] = 1;
//     in_value["mem_limit"] = 10240*3;

//     //R"()", raw string  
//     // 括号里是一个字符串，字符串里的所有字符，如果出现特殊字符，不要解释，全部当作正常字符看待
//     in_value["code"] = R"(
//     #include<iostream>
//     int main()
//     {
//         while(1);
//         std::cout << "这里是我的打印消息！！" << std::endl;
//         return 0;
//     }
//     )";

//     Json::FastWriter writer;
//     in_json = writer.write(in_value);
//     std::cout << in_json << std::endl;


//     //这个是将来给客户端返回的json串
//     std::string out_json;

//     CompileAndRun::Start(in_json, &out_json);
//     std::cout << out_json << std::endl;
//     return 0;
// }