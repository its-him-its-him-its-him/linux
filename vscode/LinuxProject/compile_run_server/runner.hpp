#pragma once


#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>


#include "../common/util.hpp"
#include "../common/log.hpp"


namespace ns_runner
{
    using namespace ns_util;
    using namespace ns_log;

    class Runner
    {
    public:
        Runner()
        {}
        ~Runner()
        {}
    public:
        static void SetProcLimit(int _cpu_limit, int _mem_limit)
        {
            // 设置CPU时长
            struct rlimit cpu_rlimit;
            cpu_rlimit.rlim_max = RLIM_INFINITY;
            cpu_rlimit.rlim_cur = _cpu_limit;
            setrlimit(RLIMIT_CPU, &cpu_rlimit);

            // 设置内存大小
            struct rlimit mem_rlimit;
            mem_rlimit.rlim_max = RLIM_INFINITY;
            mem_rlimit.rlim_cur = _mem_limit * 1024; //转化成为KB
            setrlimit(RLIMIT_AS, &mem_rlimit);
        }
    public:

        // Run返回值为 int 
        /*******************************************
         * 返回值 > 0: 程序异常了，退出时收到了信号，返回值就是对应的信号编号
         * 返回值 == 0: 正常运行完毕的，结果保存到了对应的临时文件中
         * 返回值 < 0: 内部错误,Run函数里面 - 打开文件失败和创建子进程失败return 的 是小于 0的值
         * 
         * cpu_limit: 该程序运行的时候，可以使用的最大cpu资源上限
         * mem_limit: 改程序运行的时候，可以使用的最大的内存大小(KB)
         * *****************************************/
        static int Run(const std::string& filename, int cpu_limit, int mem_limit)
        {
            /*********************************************
             * 程序运行：
             * 1. 代码跑完，结果正确
             * 2. 代码跑完，结果不正确
             * 3. 代码没跑完，异常了
             * Run需要不考虑代码跑完，结果正确与否，结果正确与否：是由测试用例决定的！
             * 只考虑：是否正确运行完毕
             * 
             * 一个程序在默认启动的时候
             * 标准输入: 不处理
             * 标准输出: 程序运行完成，输出结果是什么
             * 标准错误: 运行时错误信息
             * *******************************************/
            
            std::string _execute = PathUtil::ExeFile(filename);
            std::string _stdin = PathUtil::Stdin(filename);
            std::string _stdout = PathUtil::Stdout(filename);
            std::string _stderr = PathUtil::Stderr(filename);

            umask(0);

            int fd_stdin = open(_stdin.c_str(), O_CREAT|O_RDONLY, 0644);
            int fd_stdout = open(_stdout.c_str(), O_CREAT|O_WRONLY, 0644);
            int fd_stderr = open(_stderr.c_str(), O_CREAT|O_WRONLY, 0644);

            if(fd_stdin < 0 || fd_stdout < 0 || fd_stderr < 0)
            {
                LOG(ERROR) << "运行时打开标准文件失败" << "\n";
                return -1; // 打开文件失败
            }  

            pid_t id = fork();
            if(id < 0)
            {
                LOG(ERROR) << "运行时创建子进程失败" << "\n";
                close(fd_stdin);
                close(fd_stdout);
                close(fd_stderr);
                return -2; // 创建子进程失败
            }
            else if(id == 0)
            {
                dup2(fd_stdin, 0);
                dup2(fd_stdout, 1);
                dup2(fd_stderr, 2);
                
                // 约束进程占用太多资源(内存+时间)，防止恶意用户
                // int setrlimit(int resource, const struct rlimit *rlim);
                // struct rlimit 
                // {
                //      rlim_t rlim_cur;  /* Soft limit */
                //      rlim_t rlim_max;  /* Hard limit (ceiling for rlim_cur) */
                // };
 
                // CUP时常超时会产生 (24) SIGXCPU 信号
                // 内存申请失败会产生 (6) SIGABRT 信号
                SetProcLimit(cpu_limit, mem_limit);
                
                execl(_execute.c_str()/*我要执行谁*/, _execute.c_str()/*我想在命令行上如何执行该程序*/, nullptr);
                exit(1);                
            }
            else 
            {
                close(fd_stdin);
                close(fd_stdout);
                close(fd_stderr);
                int status = 0;
                waitpid(id, &status, 0); 


                // 程序运行异常，一定是因为因为收到了信号
                LOG(INFO) << "运行完毕, info: " << (status & 0x7F) << "\n"; 
                return status & 0x7F;                
            }
        }
    };
}