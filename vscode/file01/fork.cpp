#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

// int main()
// {
//     pid_t id = fork();
//     if(id < 0)
//     {
//         // 创建失败，返回-1
//         perror("fork");
//         exit(1);
//     }
//     else if(id == 0)
//     {
//         // 创建子进程成功
//         int count = 10;
//         while(count--)
//         {
//             cout<<"我是一个子进程！"<<endl;
//             sleep(1);
//         }
//         exit(2);
//     }
//     else
//     {
//         // 父进程执行流
//         // 1. 等待子进程
//         // 1.1 阻塞等待
//         // pid_t ret = wait(NULL);
//         // pid_t ret = waitpid(id, NULL,0); // 不获取子进程的退出结果
//         int status = 0;
//         pid_t ret = waitpid(id, &status,0); // 获取退出结果

//         if(ret > 0)
//         {
//             printf("子进程正常退出: 子进程ID : %d, 其退出受到的信号: %d, 子进程退出码: %d\n",\
//             ret, status & 0x7f, (status >> 8) & 0xff);
//         }
//         int count = 5;
//         while(count--)
//         {
//             printf("我是父进程: 其pid: %d, 其ppid: %d\n",getpid(), getppid());
//             sleep(1);
//         }
//     }
//     return 0;
// }

int main()
{
    pid_t id = fork();
    if(id < 0)
    {
        perror("fork");
        exit(1);
    }
    else if(id == 0)
    {
        int count = 4;
        while(count--)
        {
            printf("我是一个子进程，其pid: %d \n", getpid());
            sleep(1);
        }
        printf("子进程 退出！ 退出！\n");
        exit(1);
    }
    else 
    {
        // 僵尸进程，父进程不等待子进程
        while(true)
        {
            printf("我是一个父进程: 其pid: %d\n",getpid());
            sleep(1);
        }
    }
    return 0;
}