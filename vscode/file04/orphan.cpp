#include<iostream>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>
using namespace std;
// 文件操作函数
int main()
{
    FILE* fp1 = fopen("bite.txt", "w");
    // 往文件里写
    const char* s1 = "linux so easy!";
    ssize_t ret1 = fwrite(s1, sizeof(char), strlen(s1), fp1);
    assert(ret1 != 0);
    fclose(fp1);

    // 往文件里读
    FILE* fp2 = fopen("bite.txt", "r");
    char s2[1024*4];
    memset(s2, 0, sizeof(s2));
    ssize_t ret2= fread(s2, sizeof(char), sizeof(s2)-1, fp2);
    assert(ret2 != 0);
    printf("fread: %s\n", s2);
    fclose(fp2);
    return 0;
}



// 程序替换

// int main()
// {
//     pid_t id = fork();
//     if(id < 0)
//     {
//         perror("fork");
//         exit(0);
//     }
//     else if(id == 0)
//     {
//         // 执行程序替换的代码
//         printf("执行程序替换\n");
//         execl("/usr/bin/ls", "ls", "-l", "-a", NULL);
//         exit(1);
//     }

//     pid_t ret = waitpid(id, nullptr, 0);
//     if(ret > 0)
//     {
//         printf("子进程等待成功！\n");
//     }
//     printf("父进程退出!\n");
//     return 0;
// }


// int main()
// {
//     pid_t id = fork();
//     if(id < 0)
//     {
//         perror("fork()");
//         exit(0);
//     }
//     else if(id == 0)
//     {
//         int count = 5;
//         // 子进程
//         while(count--)
//         {
//             cout<<"我是子进程!-->"<<"我的pid:" << getpid() << " 其父进程pid" << getppid() <<endl;
//             sleep(1);
//         }
//         exit(1);
//     }
//     int status = 0;
//     pid_t ret = waitpid(id, &status,0); // 阻塞等待
//     if(ret > 0)
//     {
//         printf("子进程等待成功, 其PID:%d, 其退出信号:%d, 其退出码:%d\n", ret, status & 0x7f, (status >> 8) & 0xff );
//     }
//     cout<<"父进程退出!" << endl;
//     return 0;
// }



// // 演示孤儿进程
// int main()
// {
//     pid_t id = fork();
//     if(id < 0)
//     {
//         perror("fork()");
//         exit(0);
//     }
//     else if(id == 0)
//     {
//         int count = 9;
//         // 子进程
//         while(count--)
//         {
//             cout<<"我是子进程!-->"<<"我的pid:" << getpid() << " 其父进程pid" << getppid() <<endl;
//             sleep(1);
//         }
//         exit(1);
//     }
//     else 
//     {
//         int count = 3;
//         while(count--)
//         {
//             cout<<"我是父进程!-->"<<"我的pid:" << getpid() << " 其父进程pid:" << getppid()<<endl;
//             sleep(1);
//         }
//         cout << "父进程退出了!!!"<<endl;
//         exit(1);
//     }
// }

