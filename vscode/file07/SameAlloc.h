#pragma once

#include"Common.h"
#include"ThreadCache.h"
#include<thread>


static void* SameAlloc(size_t size)
{
	// 通过TLS 每个线程无锁的获取自己的专属的ThreadCache对象
	if (pTLSThreadCache == nullptr)
	{
		pTLSThreadCache = new ThreadCache;
	}

	std::cout << std::this_thread::get_id() << ":" << pTLSThreadCache << std::endl;

	return pTLSThreadCache->Allocate(size);
}

static void SameFree(void* ptr, size_t size)
{
	assert(pTLSThreadCache);

	pTLSThreadCache->Deallocate(ptr, size);
}