#include <iostream>
#include <cstring>
#include <ctime>
#include <cassert>
#include <cerrno>
#include <fcntl.h>
#include <unistd.h>

#include <sys/time.h>


bool SetNonBlock(int fd)
{
    int fl = fcntl(fd, F_GETFL); // 在底层获取当前fd对应的文件读写标志位
    if (fl < 0)
        return false;
    fcntl(fd, F_SETFL, fl | O_NONBLOCK); // 设置非阻塞
    return true;
}

int main()
{
    // 将0号文件描述符设置为非阻塞
    SetNonBlock(0); //只要设置一次，后续就都是非阻塞了

    char buffer[1024];
    while (true)
    {
        sleep(1);
        errno = 0;
        // 非阻塞的时候，我们是以出错的形式返回，告知上层数据没有就绪：通过errno来辨别出错类型
        // 0号设置为非阻塞的时候，read不再阻塞等待键盘输入内容，而是会直接返回
        ssize_t s = read(0, buffer, sizeof(buffer) - 1); 

        // s > 0 代表读取到数据了,否则代表出错了，出错包含两种，一种代表正常continue再次循环，一种代表异常
        // 出错，不仅仅是错误返回值，errno变量也会被设置，表明出错原因，可以通过errno来确定出错类型
        if (s > 0)
        {
            buffer[s-1] = 0;
            std::cout << "echo# " << buffer << " errno[---]: " << errno << " errstring: " << strerror(errno) << std::endl;
        }
        else
        {
            // 如果失败的errno值是11，代表其实没错，只不过是底层数据没就绪，非阻塞直接返回而已
            // EWOULDBLOCK 和  EAGAIN 就是 11
            if(errno == EWOULDBLOCK || errno == EAGAIN)
            {
                std::cout << "当前0号fd数据没有就绪, 请下一次再来试试吧" << std::endl;
                continue;
            }
            // EINTR 也是代表正常
            else if(errno == EINTR)
            {
                std::cout << "当前IO可能被信号中断,在试一试吧" << std::endl;
                continue;
            }
            else
            {
                //进行差错处理
            }
        }
    }
    return 0;
}

// int main()
// {
    
//     char buffer[1024];
//     while (true)
//     {
//         // 0号文件描述符，阻塞式，read会阻塞式等待用户的输入
//         ssize_t s = read(0, buffer, sizeof(buffer) - 1); 
//         if (s > 0)
//         {
//             buffer[s-1] = 0;
//             std::cout << "echo# " << buffer << std::endl;
//         }
//         else
//         {
//             std::cout << "read \"error\" " << std::endl;
//         }
//     }
//     return 0;
// }