#include <memory>
#include <iostream>
#include "SelectServer.hpp"

int main()
{
    // 1. fd_set是一个固定大小位图，直接决定了select能同时关心的fd的个数是有上限的！
    std::cout << sizeof(fd_set) * 8 << std::endl;
    std::cout<<"front --- front" <<endl;

    std::unique_ptr<SelectServer> svr(new SelectServer());
    std::cout<<"start --- start" <<endl;
    svr->Start();
    std::cout<<"end --- end" <<endl;
    
    return 0;
}