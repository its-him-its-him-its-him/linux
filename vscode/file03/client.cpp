#include "common.hpp"

using namespace std;

int main()
{
    // 1. 获取公共的key,保证进程看到同一个共享内存
    key_t k = ftok(PATH_NAME, KEY_ID);
    assert(k != -1);

    // 2. 获取共享内存
    int shmid = shmget(k, SHM_SIZE, 0);
    if(shmid == -1)
    {
        perror("shmget");
        exit(1);
    }

    // 3. 将共享内存连接到自己的地址空间
    char* shmaddr = (char*)shmat(shmid, nullptr, 0);

    // 输入数据到共享内存
    while(true)
    {
        cout<<"输入数据到共享内存: ";
        fflush(stdout);
        ssize_t s = read(0, shmaddr, SHM_SIZE-1);
        if(s > 0)
        {
            shmaddr[s - 1] = 0;
            if(strcmp(shmaddr,"quit") == 0)
                break;
        }
    }
    // 4. 去关联
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;
    return 0;
}