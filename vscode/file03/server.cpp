#include "common.hpp"

using namespace std;

int main()
{
    // 1. 创建公共的key,保证进程看到同一个共享内存
    key_t k = ftok(PATH_NAME, KEY_ID);
    assert(k != -1);

    // 2. 创建共享内存
    int shmid = shmget(k, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0666);
    if(shmid == -1)
    {
        perror("shmget");
        exit(1);
    }

    // 3. 将共享内存连接到自己的地址空间
    char* shmaddr = (char*)shmat(shmid, nullptr, 0);

    // 读取共享内存的数据
    while(true)
    {
        sleep(1);
        printf("server读取共享内存的数据:%s \n", shmaddr);
        if(strcmp(shmaddr, "quit") == 0)
            break;
    }
    // 4. 去关联
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;

    // 5. 删除共享内存
    n = shmctl(shmid, IPC_RMID, nullptr);
    assert(n != -1);
    (void)n;
    return 0;
}