#include"common.h"

using namespace std;

int main()
{
    //创建命名管道文件
    int n = mkfifo(ipcPath.c_str(), MODE);
    if(n<0)
    {
        perror("mkfifo");
        exit(0);
    }

    // 对文件的操作，读取文件内容
    // 1. 打开文件
    int fd = open(ipcPath.c_str(), O_RDONLY); 
    // 2. 读取文件内容
    pid_t id = fork();
    if(id < 0)
    {
        perror("fork");
        exit(1);
    }
    else if(id == 0)
    {
        // 子进程读
        char buff[SIZE];
        while(true)
        {
            memset(buff, '\0', sizeof(buff));
            ssize_t s = read(fd, buff, sizeof(buff)-1);
            if(s < 0)
            {
                perror("read");
                exit(2);
            }
            else if(s > 0)
            {
                printf("server接受到的内容: %s\n", buff);
            }
            else 
            {
                printf("read end of file\n");
                break;
            }
        }
        exit(3);
    }

    // 等待子进程
    pid_t ret = waitpid(id, nullptr, 0);
    // 3. 关闭文件
    close(fd);
    // 4. 管道文件删除
    unlink(ipcPath.c_str());
    cout<<" server quit! "<<endl;
    return 0;
}