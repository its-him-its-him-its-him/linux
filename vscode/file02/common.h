#pragma once

#include<iostream>
#include<stdio.h>
#include<assert.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdlib.h>

using namespace std;

#define MODE 0666
#define SIZE 128

string ipcPath = "./file.ipc";
