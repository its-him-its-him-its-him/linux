#include"common.h"

using namespace std;

int main()
{
    // 1.打开管道文件
    int fd = open(ipcPath.c_str(), O_WRONLY);
    // 2. 写文件
    char buff[] = "i am process A";
    int count = 5;
    while(count--)
    {
        cout << "client写入: i am process A " <<endl;
        write(fd, buff, sizeof(buff));
        sleep(1);
    }
    // 3. 关闭文件
    close(fd);
    cout<<"client quit!"<<endl;
    return 0;
}