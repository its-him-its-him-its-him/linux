#pragma once

#include <iostream>
#include <functional>
#include <unistd.h>
#include "Sock.hpp"
#include <signal.h>

class HttpServer
{
public:
    using func_t = std::function<void(int)>;
public:
    HttpServer(const uint16_t &port, func_t func): _port(port),_func(func)
    {
        _listensock = _sock.Socket();
        _sock.Bind(_listensock, _port);
        _sock.Listen(_listensock);
    }

    void Start()
    {
        signal(SIGCHLD, SIG_IGN);
        for( ; ; )
        {
            std::string clientIp;
            uint16_t clientPort = 0;
            int servicesock = _sock.Accept(_listensock, &clientIp, &clientPort);
            if(servicesock < 0) continue;
            if(fork() == 0)
            {
                close(_listensock);
                _func(servicesock);
                close(servicesock);
                exit(0);
            }
            close(servicesock);
        } 
    }
private:
    int _listensock;
    uint16_t _port;
    Sock _sock;
    func_t _func;
};