#include<iostream>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>
using namespace std;

int main()
{
    /// 系统接口open
    int fd = open("bite.txt", O_RDWR | O_CREAT, 0666); 
    if(fd < 0)
    {
        perror("open");
        return 1;
    }

    /// 系统接口write
    const char *s = "linux so easy!\n";
    write(fd, s, strlen(s));

    /// 系统接口read
    off_t len = lseek(fd, 0, SEEK_CUR);
    lseek(fd, -len, SEEK_CUR);
    char buffer[64];
    memset(buffer, '\0', sizeof(buffer));
    read(fd, buffer, sizeof(buffer));
    printf("read: %s", buffer);
    // 系统接口close
    close(fd); 
}

// 文件操作函数
int main()
{
    FILE* fp1 = fopen("bite.txt", "w");
    // 往文件里写
    const char* s1 = "linux so easy!";
    ssize_t ret1 = fwrite(s1, sizeof(char), strlen(s1), fp1);
    assert(ret1 != 0);
    fclose(fp1);

    // 往文件里读
    FILE* fp2 = fopen("bite.txt", "r");
    char s2[1024*4];
    memset(s2, 0, sizeof(s2));
    ssize_t ret2= fread(s2, sizeof(char), sizeof(s2)-1, fp2);
    assert(ret2 != 0);
    printf("fread: %s\n", s2);
    fclose(fp2);
    return 0;
}

// // 文件操作函数
// int main()
// {
//     // 往文件里读写
//     FILE* fp = fopen("bite.txt", "w+");
//     const char* s1 = "linux so easy!";
//     ssize_t ret1 = fwrite(s1, sizeof(char), strlen(s1), fp);
//     assert(ret1 != 0);

//     rewind(fp);//返回文件首位 (注意 注意 一定要将文件指针回位)

//     char s2[1024*4];
//     memset(s2, 0, sizeof(s2));
//     ssize_t ret2= fread(s2, sizeof(char), sizeof(s2)-1, fp);
//     assert(ret2 != 0);
//     printf("fread: %s\n", s2);
//     fclose(fp);
//     return 0;
// }

// // 文件操作函数
// int main()
// {
//     // 往文件里读写
//     FILE* fp = fopen("bite.txt", "r+");

//     char s2[1024*4];
//     memset(s2, 0, sizeof(s2));
//     ssize_t ret2= fread(s2, sizeof(char), sizeof(s2)-1, fp);
//     assert(ret2 != 0);
//     printf("fread: %s\n", s2);

//     const char* s1 = "linux so easy!\n";
//     ssize_t ret1 = fwrite(s1, sizeof(char), strlen(s1), fp);
//     assert(ret1 != 0);

//     fclose(fp);
//     return 0;
// }



