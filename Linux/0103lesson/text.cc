#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <errno.h>
void myperror(const char* msg)
{
      fprintf(stderr, "%s: %s\n", msg, strerror(errno));

}
int main()
{

  int fd = open("log.txt", O_RDONLY);
  if (fd < 0)
  {
            myperror("open");
            return 1;

  }

   // stdout -> 1
   printf("hello printf 1\n");
   fprintf(stdout,"hello fprintf 1\n");
   // stderr -> 2
  
   errno = 1;
   perror("hello perror 2"); //stderr
  
   const char *s1 = "hello write 1\n";
   write(1, s1, strlen(s1));
  
   const char *s2 = "hello write 2\n";
   write(2, s2, strlen(s2));
  
   // cout -> 1
   std::cout << "hello cout 1" << std::endl;
   // cerr -> 2
   std::cerr << "hello cerr 2" << std::endl;
  
  
  
  

  
  
//      close(1);
//          int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//              if(fd < 0)
//              {
//                        perror("open");
//                                return 0;
//                                    
//              }
//
//                  printf("hello printf: %d\n", fd); 
//                  fflush(stdout);
//                  const char *msg = "hello write\n";
//                          write(fd, msg, strlen(msg));
//
//                             close(fd); 
//                                return 0;
//
}
