#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#define NUM 102
int main()
{
  // 定义一个数组存放字符
  char bar[NUM];
  const char* lable = "|/-\\";
  // 初始化数组
  memset(bar, 0, sizeof(bar));
  int cnt = 0;
  while(cnt<=100)
  {
    printf("[%-100s][%d%%]  %c\r", bar, cnt, lable[cnt % 4]);
    bar[cnt++] = '#';
    fflush(stdout);
    sleep(1);
  }
  printf("\n");
  return 0;
}
