#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
// main函数有三个参数
// int argc     存放命令行参数的个数 (argc / argv )
// char* argv[] 命令行参数 字符数组,每个元素都是一个指针，指向一个字符串
// char* env[]  环境变量,子进程的环境变量是从父进程来的，默认所有的环境变量都会被子进程继承
int main(int argc, char* argv[], char* env[])
{
  int n = 0;
  for(n = 0; n < argc; n++)
  {
    printf("argv[%d] %s\n", n, argv[n]);
  }
  ////////////////////////////////////////////////////////////////// ///////
  int i = 0;
  // 通过命令行参数获取环境变量
  printf("begin-----------------------------------------\n");
  for(i = 0; env[i]; i++)
  {
    printf("env[%d]: %s\n",i , env[i]);
  }
  
  printf("end-----------------------------------------\n");
  
  // 通过第三方获取环境变量
  extern char** environ; // environ不要写错了 
  printf("begin-----------------------------------------\n");
  for(i = 0; environ[i]; i++)
  {
    printf("envrion[%d]: %s\n",i , environ[i]);
  }
  
  printf("end-----------------------------------------\n");

  // 通过getenv(setenv)获取环境变量
  printf("getenv: %s\n", getenv("PATH"));// 根据环境变量名获取
 
  return 0;
}
