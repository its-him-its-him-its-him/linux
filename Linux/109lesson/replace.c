#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/wait.h>

int main(int argc, char* argv[], char* env[])
{
  printf("当前进程开始的代码\n");
  // execl 第一个参数，是为了找到程序，路径要写全
  //execl("/usr/bin/ls", "ls", NULL);
  //execl("/usr/bin/ls", "ls","-a", "-l", NULL);
  
  //execl("/usr/bin/top", "top", NULL);
  
  // execlp 带p, 与execl不同的是，不需要写全路径，自己会去环境变量中去找
  //execlp("ls", "ls", NULL);
  //execlp("ls", "ls", "-a", "-l", NULL);
  //execlp("top", "top", NULL);
  
  // execle 不带p, 路径要写全， 而且有第三个参数，环境变量自己维护
  //execle("/usr/bin/ls","ls", NULL, env); // 环境变量使用父进程main提供的
  
  char* _env[] = {"NULL"};

  execle("/usr/bin/ls","ls", NULL, _env); 
 
  printf("当前进程结束的代码\n");
  return 0;
}
