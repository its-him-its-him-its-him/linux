#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<sys/wait.h>
int main()
{
  printf("我是一个父进程\n");

  pid_t id = fork();
  if(id < 0)
  {
    perror("fork");
    exit(1);
  }
  else if(id == 0)
  {
    // 子进程
    int cnt = 5;
    while(cnt)
    {
      printf("我是一个子进程，我的PID: %d, PPID %d\n", getpid(), getppid());
      sleep(1);
      cnt--;
      int*p;
      *p = 10;
    }
    exit(2);
  }
  else 
  {
    
    // 父进程
    printf("我是一个父进程，我的PID: %d, PPID: %d\n", getpid(), getppid());
    //pid_t ret = wait(NULL); // 阻塞式等待
    // pid_t ret waitpid(id, NULL,0); 
    int status = 0;
    pid_t ret = waitpid(id, &status, 0);
//    if(ret > 0)
//    {
//      printf("等待子进程成功， ret: %d, 子进程收到的信号编号:%d, 之进程的退出码:%d\n",ret,
//          status & 0x7f, (status>>8) &  0xff);
//    }
    // 等待子进程成功，还要判断进程退出的退出码，查看进程是否正常退出
    if(ret > 0)
    {
      if(WIFEXITED(status))
      {
        printf("子进程正常退出，退出码是: %d\n", WEXITSTATUS(status));
      }
      else 
      {

        printf("子进程异常退出，退出码是: %d\n", WIFEXITED(status));
        printf("子进程异常退出，退出码是: %d\n", WEXITSTATUS(status));
      }
    }
    // 阻塞式等待/子进程结束waite获取结果后，父进程在被调度执行
    while(1)
    {
      printf("我是父进程，pid:%d, ppid: %d\n", getpid(), getppid());
      sleep(1);
    }
  }
  return 0;
}
