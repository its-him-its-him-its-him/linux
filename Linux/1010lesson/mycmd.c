#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>

// ./mycmd -a/ -b/ -c
int main(int argc, char* argv[])
{
  if(argc != 2)
  {
     printf("can not execute!\n");
     exit(1);
  }

  // MY_105_VAL: 需要从main函数中获取
  printf("获取环境变量: MY_105_VAL: %s\n", getenv("MY_105_VAL"));

  if(strcmp(argv[1], "-a") == 0)
  {
    printf("hello a\n");
  }
  else if(strcmp(argv[1], "-b") == 0)
  {
    printf("hello b\n");
  }
  else if(strcmp(argv[1], "-c") == 0)
  {
    printf("hello c\n");
  }
  else 
  {
    printf("helloc default!\n");
  }
  return 0;
}
