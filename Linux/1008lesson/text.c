#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main()
{
  printf("我是一个父进程\n");

  // 创建一个子进程
  pid_t id = fork();
  if(id < 0)
  {
    // 创建失败
    perror("fork()");
    return 0;
  }
  else if(id == 0)
  {
    // 创建子进程成功
    while(1)
    {
      printf("我是一个子进程，我的PID: %d, PPID: %d\n", getpid(), getppid());
      sleep(1);
    }
  }
  else 
  {
    while(1)
    {
      // 这里是父进程
      printf("我是一个父进程，我的PID: %d, PPID: %d\n", getpid(), getppid());
      sleep(1);
    }
  }
  return 0;
}
