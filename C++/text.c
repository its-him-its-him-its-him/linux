#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

int g_unval;
int g_val = 100;

int main(int argc, char* argv[], char* env[])
{ 
  // 字面常量
  const char* str = "helloworld";

  printf("code addr: %p\n", main);
  printf("init global addr: %p\n", &g_val);
  printf("uninit galobal addr: %p\n", &g_unval);
  
  static int data = 20;
  char* heap_mem = (char*)malloc(10);
  char* heap_mem1 = (char*)malloc(10);
  char* heap_mem2 = (char*)malloc(10);
  char* heap_mem3 = (char*)malloc(10);
  printf("heap addr: %p\n", heap_mem);
  printf("heap addr: %p\n", heap_mem1);
  printf("heap addr: %p\n", heap_mem2);
  printf("heap addr: %p\n", heap_mem3);

  
  printf("data stack addr: %p\n", &data);
  printf("stack addr: %p\n", &heap_mem);
  printf("stack addr: %p\n", &heap_mem1);
  printf("stack addr: %p\n", &heap_mem2);
  printf("stack addr: %p\n", &heap_mem3);

  printf("read only string addr: %p\n",str);
  int i = 0;
  for(i = 0;i < argc; i++)
  {
    printf("argv[%d]: %p\n", i, argv[i]);
  }
  
  for(i = 0; env[i]; i++)
  {
    printf("env[%d]: %p\n", i, env[i]);
  }
  
  return 0;
}
