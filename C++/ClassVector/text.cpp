#include"vector.h"

void TextVector1()
{
  sz::vector<int> v;
  v.push_back(1);
  v.push_back(2);
  v.push_back(3);
  v.push_back(4);

  sz::vector<int>::iterator it = v.begin();
  while(it != v.end())
  {
    cout<<*it<<" ";
    it++;
  }
  cout<<endl;
}

void TextVector2()
{
  sz::vector<int> v1;
  v1.push_back(1);
  v1.push_back(2);
  v1.push_back(3);
  v1.push_back(4);
  // 拷贝构造
  sz::vector<int> v2; 
      
  v2.push_back(11);
  v2.push_back(22);
  v2.push_back(33);
  v2.push_back(44);
  sz::vector<int> v3(v1);
  sz::vector<int>::iterator it = v3.begin();
  while(it != v3.end())
  {
    cout<<*it<<" ";
    ++it; 
  }
  cout<<endl;
  v3 = v2;
  sz::vector<int>::iterator pos = std::find(v3.begin(),v3.end(),11);
  v3.insert(pos, 111);
  pos = std::find(v3.begin(),v3.end(),44);
  v3.erase(pos);
  it = v3.begin();
  while(it != v3.end())
  {
    cout<<*it<<" ";
    ++it;
  }
  cout<<endl;
}
int main()
{
  TextVector2();
  return 0;
}
