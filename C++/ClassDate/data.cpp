#include<iostream>
#include<assert.h>
using namespace std;

class Date
{
  public:
    void Print()
    {
      cout<<_year<<"年"<<_month<<"月"<<_day<<"日"<<endl;
    }
    int GetMonthDays(int year, int month)
    {
      static int days[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
      int day = days[month];
      if(month == 2
          &&((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
      {
        return day += 1;
      }
        return day; 
    }
    Date(int year = 2022, int month = 9, int day = 23)
    {
     //cout<<year<<" "<<month<<" "<<day<<" "<<endl;
      _year = year;
      _month = month;
      _day = day;
    }
    // d1(d2)
    Date(const Date& d)
    { 
     // cout<<"Date(const Date& d"<<endl;      
      {
        _year = d._year;
        _month = d._month;
        _day = d._day;
      }
    }
    
    // d1 =d2 
    Date& operator=(const Date& d) 
    {
      if(this != &d)
      {
        _year = d._year;
        _month = d._month; 
        _day = d._day; 
      }
      return *this; 
    }
    ~Date()
    {
      cout<<"~Date()"<<endl;
    }
    // 2022 9 10  + 100
    Date& operator+=(int days)
    {
      _day += days;
      while(_day > GetMonthDays(_year, _month))
      {
        _day -= GetMonthDays(_year, _month);
        _month += 1;
        if(_month == 13)
        {
          _year += 1;
          _month = 1;
        }
      }

      return *this; 
    }

    Date operator+(int days)
    {
      Date tmp(*this);
      tmp += days;

      return tmp;
    }
  
    Date& operator-=(int days)
    {
      _day -= days;
      while(_day <= 0)
      {
        _day += GetMonthDays(_year, _month);
        _month -= 1;
        if(_month == 0)
        {
          _year -= 1;
          _month = 12;
        }
      }

      return *this; 
    }

    Date operator-(int days)
    {
      Date tmp(*this);
      tmp -= days;

      return tmp;
    }
    // 前置++
    Date& operator++()
    {
      *this += 1;
      return *this;
    }

    // 后置++
    Date operator++(int)
    {
      Date tmp(*this);
      *this += 1;

      return tmp;
    }

    // 前置--
    Date& operator--()
    {
      *this -= 1;
      return *this;
    }

    // 后置--
    Date operator--(int)
    {
      Date tmp(*this);
      *this -= 1;
      return tmp;
    }
 // 运算符重载
 // d1>=d2 -> d1.operator(&d1, &d2) 
 bool operator>(const Date& d)
 {
   if(_year > d._year 
     ||(_year <= d._year && _month > d._month)
     ||(_year <= d._year && _month <= d._month && _day > d._day))
     {
       return true;
     }
   return false;
 }

 // d1 == d2 
 bool operator==(const Date& d)
 {
   if(_year == d._year && _month == d._month && _day == d._day)
   {
     return true; 
   }
   return false; 
 }
 
 
 bool operator<=(const Date& d)
 {
   return !(*this > d);
 }
 bool operator>=(const Date& d)
 {
   return (*this > d) && (*this == d);
 }
 bool operator!=(const Date& d)
 {
   return !(*this == d);
 }
 bool operator<(const Date& d)
 {
   return !(*this > d || *this == d);
 }

 int operator-(const Date& d)
 {
   Date max(*this);
   Date min(d);
   if(max < d)
   {
     max = d;
     min = *this;
   }
   int count = 0;
   while(max > min)
   {
    ++min;
    count++;
   }
  return count;
 }
  private:
    int _year;
    int _month;
    int _day;

};

void TextDate1()
{

  Date d1;
  Date d2(1,1,1);
}

void TextDate2()
{
  Date d1(2022, 9, 24);
  d1 += 10;
  d1.Print();
  d1 += 100;
  d1.Print();
  d1 += 1000;
  d1.Print(); 

  (d1 + 1).Print();
  
}

void TextDate3()
{
  Date d1(2022, 9, 24);
  d1 -= 10;
  d1.Print();
  d1 -= 100;
  d1.Print();
  d1 -= 1000;
  d1.Print(); 

  (d1 + 1).Print();
}

void TextDate4()
{
  Date d1(2022, 9, 24);
 
  ++d1;
  d1.Print();
  --d1;
 (d1++).Print();
  d1.Print();
  (d1--).Print();
  d1.Print();

}

void TextDate5()
{
  Date d1(2,2,2);
  Date d2(2,2,2);
  Date d3(2,2,4);
  cout<<(d1 == d2)<<endl;
  cout<<(d1 >= d2)<<endl;
  cout<<(d1 >= d3)<<endl;

  Date d4(2022,9,24);
  Date d5(2023,9,10);
  cout<<(d5-d4)<<endl;
}
int main()
{
  TextDate5();
  return 0;
}
