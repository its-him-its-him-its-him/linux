#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
//``int main()
//``{
//``  printf("你可以看到这里的打印信息码?");
//``  sleep(3);
//``  _exit(13);
//``}
//
int main()
{
  int i = 0;
  for(i = 0; i < 100; i++) // i < 100 是打印出100条，这里的数值可以增加
  {
    printf("%d: %s\n",i, strerror(i));
  }
  return 0;
}
